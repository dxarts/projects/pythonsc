PythonPlot : Python {
	var pythonFilePath, command, tempPath;

	*new {
		^super.new
	}

	plot { |data, title = "Python Plot", xLabel = "Time", yLabels = (["Axis1"]), verbose = false, tempPath, removeTemp = true|
		tempPath = tempPath ?? { Platform.defaultTempDir +/+ "PythonPlotData" ++ Main.elapsedTime ++ ".txt" };
		pythonFilePath = Python.utilPath.escapeChar($ ) +/+ "Plot/Plot.py";
		this.saveForPython(data, tempPath);
		command = Python.pathToPython + pythonFilePath.escapeChar($ ) + tempPath.escapeChar($ ) + title.asString.shellQuote + xLabel.asString.shellQuote + yLabels.asCompileString.shellQuote;
		verbose.if({ command.postln });
		command.unixCmd({ |err|
			verbose.if({ err.postln });
			removeTemp.if({ File.delete(tempPath) })
		});

	}

	plot3D { |data, title = "Python Plot", labels = (["Axis1", "Axis2", "Axis3"]), verbose = false, tempPath, removeTemp = true|
		tempPath = tempPath ?? { Platform.defaultTempDir +/+ "PythonPlotData" ++ Main.elapsedTime ++ ".txt" };
		pythonFilePath = Python.utilPath.escapeChar($ ) +/+ "Plot/Plot3D.py";
		this.saveForPython(data, tempPath);
		command = Python.pathToPython + pythonFilePath.escapeChar($ ) + tempPath.escapeChar($ ) + title.asString.shellQuote + labels.asCompileString.shellQuote;
		verbose.if({ command.postln });
		command.unixCmd({ |err|
			verbose.if({ err.postln });
			removeTemp.if({ File.delete(tempPath) })
		})

	}

	histogram { |data, title = "Python Histogram", xLabels = (["Axis1"]), verbose = false, tempPath, removeTemp = true|
		tempPath = tempPath ?? { Platform.defaultTempDir +/+ "PythonPlotData" ++ Main.elapsedTime ++ ".txt" };
		pythonFilePath = Python.utilPath.escapeChar($ ) +/+ "Plot/Histogram.py";
		this.saveForPython(data, tempPath);
		command = Python.pathToPython + pythonFilePath.escapeChar($ ) + tempPath.escapeChar($ ) + title.asString.shellQuote + xLabels.asCompileString.shellQuote;
		verbose.if({ command.postln });
		command.unixCmd({ |err|
			verbose.if({ err.postln });
			removeTemp.if({ File.delete(tempPath) })
		});

	}

}