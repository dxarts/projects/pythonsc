PythonAnalysis {
	var <>type, <>sampleRate, <>pathToPython, <>dataFolder;
	var <>scriptsFolder, <>python;
	classvar <essentiaTypes;

	*initClass {
		essentiaTypes = IdentityDictionary.new(know: true).putPairs([
			'mfcc', 'mfcc',
			'specPcile', 'spectral_rolloff',
			'loudness', 'spectral_energy',
			'specCentroid', 'spectral_centroid',
			'specFlatness', 'spectral_flatness_db'
		])
	}

	*new { |type = 'essentia', sampleRate, pathToPython|
		^super.newCopyArgs(type, sampleRate, pathToPython).init
	}

	init {
		sampleRate = sampleRate ?? { 48000 };
		scriptsFolder = Python.utilPath +/+ "PythonAnalysis";
		dataFolder = scriptsFolder +/+ "TmpData";
		dataFolder.mkdir;
		this.python_(Python.new(pathToPython))
	}

	consolidateData { |analysisTypes, verbose = false|
		var cmd, savePath, data;
		savePath = dataFolder +/+ "dataTemp.npy";
		cmd = (this.python.python ?? { Python.pathToPython }) + scriptsFolder.escapeChar($ ) +/+ "Data.py" + dataFolder.escapeChar($ ) + savePath.escapeChar($ ) + "-a";
		analysisTypes.do{ |analysisType|
			cmd = cmd + analysisType
		};
		verbose.if({ cmd.postln });
		cmd.systemCmd;



		// PathName(dataFolder).files.do{ |pathname|
		// 	File.delete(pathname.fullPath)
		// };
		//
		// File.delete(savePath);

		^data
	}


	files {
		^CSVFileReader.read(dataFolder +/+ "Files.csv").flatten
	}

	fft { |filePath, fftSize, hopSize, verbose = false, args|
		var cmd, data, file;
		file = dataFolder +/+ UniqueID.next ++ 'fft.npy';
		cmd = (this.python.python ?? { Python.pathToPython }) + scriptsFolder.escapeChar($ ) +/+ type ++ ".FFT.py" + filePath.escapeChar($ ) + file.escapeChar($ ) + "-s" + sampleRate + "-f" + fftSize + "-l" + (hopSize * fftSize).asInteger;
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		data = python.fromNumpy(file, verbose);
		File.delete(file);
		^data

	}

	specCentroid { |filePath, fftSize, hopSize, verbose = false, args|
		var cmd, data, file;
		file = dataFolder +/+ UniqueID.next ++ 'specCentriod.npy';
		cmd = (this.python.python ?? { Python.pathToPython }) + scriptsFolder.escapeChar($ ) +/+ type ++ ".SpecCentroid.py" + filePath.escapeChar($ ) + file.escapeChar($ ) + "-s" + sampleRate + "-f" + fftSize + "-l" + (hopSize * fftSize).asInteger;
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		data = python.fromNumpy(file, verbose);
		File.delete(file);
		^data.flat
	}

	specFlatness { |filePath, fftSize, hopSize, verbose = false, args|
		var cmd, data, file;
		file = dataFolder +/+ UniqueID.next ++ 'specFlatness.npy';
		cmd = (this.python.python ?? { Python.pathToPython }) + scriptsFolder.escapeChar($ ) +/+ type ++ ".SpecFlatness.py" + filePath.escapeChar($ ) + file.escapeChar($ ) + "-s" + sampleRate + "-f" + fftSize + "-l" + (hopSize * fftSize).asInteger;
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		data = python.fromNumpy(file, verbose);
		File.delete(file);
		^data.flat
	}

	specPcile { |filePath, fftSize, hopSize, fraction = 0.9, verbose = false, args|
		var cmd, data, file;
		file = dataFolder +/+ UniqueID.next ++ 'specPcile.npy';
		args !? { fraction = args[0] };
		cmd = (this.python.python ?? { Python.pathToPython }) + scriptsFolder.escapeChar($ ) +/+ type ++ ".SpecPcile.py" + filePath.escapeChar($ ) + file.escapeChar($ ) + "-s" + sampleRate + "-f" + fftSize + "-l" + (hopSize * fftSize).asInteger + "-p" + fraction;
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		data = python.fromNumpy(file, verbose);
		File.delete(file);
		^data.flat
	}

	loudness { |filePath, fftSize, hopSize, verbose = false, args|
		var cmd, data, file;
		file = dataFolder +/+ UniqueID.next ++ 'loudness.npy';
		cmd = (this.python.python ?? { Python.pathToPython }) + scriptsFolder.escapeChar($ ) +/+ type ++ ".Loudness.py" + filePath.escapeChar($ ) + file.escapeChar($ ) + "-s" + sampleRate + "-f" + fftSize + "-l" + (hopSize * fftSize).asInteger;
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		data = python.fromNumpy(file, verbose);
		File.delete(file);
		^data.flat
	}

	duration { |filePath, verbose = false, args|
		var cmd, data, file;
		file = dataFolder +/+ UniqueID.next ++ 'duration.npy';
		cmd = (this.python.python ?? { Python.pathToPython }) + scriptsFolder.escapeChar($ ) +/+ type ++ ".Duration.py" + filePath.escapeChar($ ) + file.escapeChar($ );
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		data = python.fromNumpy(file, verbose);
		File.delete(file);
		^data
	}

	essentia { |filePath, savePath, analysisTypes, fftSize, hopSize, verbose = false|
		var cmd, data, file;
		file = dataFolder +/+ UniqueID.next ++ 'essentia.npy';
		cmd = (this.python.python ?? { Python.pathToPython }) + scriptsFolder.escapeChar($ ) +/+ "essentia.Extractor.py" + filePath.escapeChar($ ) + file.escapeChar($ ) + "-s" + sampleRate + "-f" + fftSize + "-l" + (hopSize * fftSize).asInteger + "-a";
		analysisTypes.do{ |analysisType|
			// if we need to translate analysis types
			essentiaTypes[analysisType].notNil.if({
				cmd = cmd + essentiaTypes[analysisType]
			}, { // otherwise, assume it is valid
				cmd = cmd + analysisType
			})
		};
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		data = python.fromNumpy(file, verbose);
		File.delete(file);
		^data
	}

	essentiaAll { |filePath, savePath, featurePath, fftSize, hopSize, verbose = false|
		var cmd, data, file;
		file = dataFolder +/+ UniqueID.next ++ 'duration.npy';
		cmd = (this.python.python ?? { Python.pathToPython }) + scriptsFolder.escapeChar($ ) +/+ "essentia.ExtractorAll.py" + filePath.escapeChar($ ) + file.escapeChar($ ) + featurePath.escapeChar($ ) + "-s" + sampleRate + "-f" + fftSize + "-l" + (hopSize * fftSize).asInteger;
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		data = python.fromNumpy(file, verbose);
		File.delete(file);
		^data
	}

	fftPlot { |data|
		SpectrogramPlot.new(data: data).plot
	}

}