PythonML : Python {
	var <>saveModelPath, <prediction, <scaler, <length, <scaledDataFolder;
	var <mean, <std, <tmpPaths;

	*new { |pathToPython|
		^super.newCopyArgs(pathToPython)
	}

	train { |trainData, trainResult, type = "KNN", args, modelPath, scalerPath, scalerType = "MinMax", scaledDataPath, axes, verbose = false|
		var resultPath, resultPathNPY, cmd;
		saveModelPath = modelPath ?? { Platform.defaultTempDir +/+ "Python" ++ type ++ "Model" ++ UniqueID.next ++ ".sav" };
		resultPath = Platform.defaultTempDir +/+ "Python" ++ type ++ "Result.txt";
		resultPathNPY = resultPath.replaceExtension("npy");
		tmpPaths = tmpPaths.add(resultPath).add(resultPathNPY);
		this.saveForPython(trainResult, resultPath);
		cmd = (this.python ?? { Python.pathToPython }) + Python.utilPath.escapeChar($ ) +/+ "ML" +/+ type ++ "Train.py" + scaledDataPath.escapeChar($ ) + resultPathNPY.escapeChar($ ) + saveModelPath.escapeChar($ );
		args.notNil.if({
			args.do{ |thisArg|
				cmd = cmd + thisArg
			}
		});
		this.toNumpy(resultPath, resultPathNPY, verbose);
		this.scale(trainData, scalerType, scalerPath, scaledDataPath, axes, verbose);
		verbose.if({ cmd.postln });
		cmd.systemCmd;

	}

	score { |trainData, trainResult, type = "KNN", args, modelPath, scalerPath, scalerType = "MinMax", scaledDataPath, axes, verbose = false|
		var resultPath, resultPathNPY, cmd;
		saveModelPath = modelPath ?? { Platform.defaultTempDir +/+ "Python" ++ type ++ "Model" ++ UniqueID.next ++ ".sav" };
		resultPath = Platform.defaultTempDir +/+ "Python" ++ type ++ "Result.txt";
		resultPathNPY = resultPath.replaceExtension("npy");
		tmpPaths = tmpPaths.add(resultPath).add(resultPathNPY);
		this.saveForPython(trainResult, resultPath);
		cmd = (this.python ?? { Python.pathToPython }) + Python.utilPath.escapeChar($ ) +/+ "ML" +/+ type ++ "TestModel.py" + scaledDataPath.escapeChar($ ) + resultPathNPY.escapeChar($ ) + saveModelPath.escapeChar($ );
		args.notNil.if({
			args.do{ |thisArg|
				cmd = cmd + thisArg
			}
		});
		this.toNumpy(resultPath, resultPathNPY, verbose);
		this.scale(trainData, scalerType, scalerPath, scaledDataPath, axes, verbose: verbose);
		verbose.if({ cmd.postln });
		cmd.systemCmd

	}

	predict { |dataPath, modelPath, scalerPath, dataLength, verbose = false|
		var cmd, resultPath;
		saveModelPath = modelPath ?? { saveModelPath };
		File.exists(saveModelPath).if({
			resultPath = Platform.defaultTempDir +/+ "PythonReturn.txt";
			tmpPaths = tmpPaths.add(resultPath);
			scaler = scalerPath ?? { scaler };
			dataLength = dataLength ??  { this.dataLength };
			cmd = (this.python ?? { Python.pathToPython }) + Python.utilPath.escapeChar($ ) +/+ "ML/Predict.py" + dataPath.escapeChar($ ) + saveModelPath.escapeChar($ ) + resultPath.escapeChar($ ) + "-s" + scaler + "-l" + dataLength;
			verbose.if({ cmd.postln });
			cmd.systemCmd;
			^CSVFileReader.read(resultPath)
		}, {
			"Please train the model first".warn
		})

	}

	predictDTW { |testData, trainData, action, verbose = false|
		var cmd, resultPath;
		resultPath = Platform.defaultTempDir +/+ "PythonReturn.txt";
		tmpPaths = tmpPaths.add(resultPath);
		cmd = (this.python ?? { Python.pathToPython }) + Python.utilPath.escapeChar($ ) +/+ "ML/dtw.py" + trainData.escapeChar($ ) + testData.escapeChar($ ) + resultPath.escapeChar($ );
		verbose.if({ cmd.postln });
		cmd.systemCmd
		^CSVFileReader.read(resultPath);

	}

	scale { |dataPath, scalerType = "MinMax", scalerPath, scaledDataPath, axes, verbose = false|
		var cmd;
		scaler = scalerPath ?? { Platform.defaultTempDir +/+ "Python" ++ scalerType ++ "Scaler.sav" };
		scaledDataFolder = scaledDataPath ?? { scaler.dirname +/+ "ScaledData.npy" };
		cmd = (this.python ?? { Python.pathToPython }) + Python.utilPath.escapeChar($ ) +/+ "ML" +/+ scalerType ++ "Scaler.py" + dataPath.escapeChar($ ) + scaler.escapeChar($ ) + scaledDataFolder.escapeChar($ );
		axes !? {
			cmd = cmd + "-a";
			axes.do{ |ax| cmd = cmd + ax };
		};
		verbose.if({ cmd.postln });
		cmd.systemCmd

	}

	knnTrain { |trainData, resultData, nNeighbors = 1, modelPath, scalerPath, scalerType = "MinMax", scaledDataPath, lengthPath, verbose = false|
		this.train(trainData, resultData, "KNN", ["-n", nNeighbors], modelPath, scalerPath, scalerType, scaledDataPath, lengthPath, verbose)
	}

	dlTrain { |trainData, resultData, hiddenLayerSizes = ([10, 10]), maxIter = 500, modelPath, scalerPath, scalerType, scaledDataPath, lengthPath, verbose = false|
		this.train(trainData, resultData, "DL", ["-l"] ++ hiddenLayerSizes ++ ["-m", maxIter], modelPath, scalerPath, scalerType, scaledDataPath, lengthPath, verbose)
	}

	dataLength { |lengthFolder|
		var path = lengthFolder ?? { length } +/+ "length.csv";
		^File.exists(path).if({
			CSVFileReader.read(path).asFloat[0][0].asInteger
		})
	}

	cleanup {
		tmpPaths.do{ |thisPath| File.delete(thisPath) }
	}


}
/*
p = PythonML.new

p.scale("/Users/dan/Library/Application\ Support/SuperCollider/tmp/PythonDeepLearningTrain.txt", scalerPath: "/Users/dan/Projects/SoundMaps/Test/Models/MinMaxScaler.sav", verbose: true)

*/