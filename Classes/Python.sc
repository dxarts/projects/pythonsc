Python {
	var >python;
	classvar <>pathToPython, <>utilPath;

	*initClass {
		utilPath = File.realpath(Python.filenameSymbol.asString.dirname.dirname ++ "/Python") ++ "/";
		pathToPython = "which python3".unixCmdGetStdOut.replace($\n);
	}

	*new { |pathToPython|
		^super.newCopyArgs(pathToPython)
	}

	python { ^python ?? { Python.pathToPython } }

	saveForPython { |data, filePath|
		var file;
		filePath = filePath ?? { Platform.defaultTempDir +/+ "forPython.txt" };
		file = File.new(filePath, "w");
		data.do{ |thisEvent|
			file.write(thisEvent.asCompileString ++ "\n")
		};
		file.close
	}

	csvSaveForPython { |data, folderPath|

		data.do{ |fileData, i|
			this.writeArray2D(fileData, folderPath ++ i ++ ".csv")
		}

	}

	writeArray2D { |array2D, filePath, delimiter = ","|
		var file;
		file = File.new(filePath, "w");
		array2D.do{ |array,i|
			array.do{ |data, j|
				file.write( data.asString );
				if ( j < ( array.size - 1) ){
					file.write( delimiter );
				};
			};
			file.write("\n");
		};
		file.close
	}

	fromNumpy { |dataPath, verbose = true|
		var cmd, data;
		cmd = this.python + Python.utilPath.escapeChar($ ) +/+ "Utilities/fromNumpy.py" + dataPath.escapeChar($ );
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		data = TabFileReader.read(dataPath.replaceExtension("tsv"));
		// File.delete(dataPath.replaceExtension("tsv"));
		data.postln;
		^(data[0][0].contains(",")).if({
			data.deepCollect(data.rank-1, {|item| item.drop(1).drop(-1).split($,)}).asFloat
		}, {
			data.asFloat
		})
	}

	toNumpy { |dataPath, savePath, verbose = true|
		var cmd;
		cmd = this.python + Python.utilPath.escapeChar($ ) +/+ "Utilities/toNumpy.py" + dataPath.escapeChar($ ) + savePath.escapeChar($ );
		verbose.if({ cmd.postln });
		cmd.systemCmd
	}

	files { |filesPath, savePath, verbose = true|
		var cmd, data;
		cmd = this.python + Python.utilPath.escapeChar($ ) +/+ "Utilities/parse_folder.py" + filesPath.escapeChar($ ) + savePath.escapeChar($ );
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		^CSVFileReader.read(savePath.replaceExtension("csv")).flatten
	}


}