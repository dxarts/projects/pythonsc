import sys
import essentia
import essentia.standard as es
w = es.Windowing(type = 'hann')

# Compute all features, aggregate only 'mean' and 'stdev' statistics for all low-level, rhythm and tonal frame features
features, features_frames = es.MusicExtractor(lowlevelStats=['mean', 'stdev'],
                                              rhythmStats=['mean', 'stdev'],
                                              tonalStats=['mean', 'stdev'])('/Users/dan/Projects/SoundMaps/test/samples/tren_44k/tren_44k_0177.294921.wav')

# See all feature names in the pool in a sorted order
print(sorted(features.descriptorNames()))

features_frames.descriptorNames()
features_frames['lowlevel.spectral_rolloff']

features['lowlevel.spectral_rolloff.mean']

loader = es.MonoLoader(filename='/Users/dan/Projects/SoundMaps/test/samples/tren_44k/tren_44k_0177.294921.wav')

# and then we actually perform the loading:
audio = loader()

spectrum = es.Spectrum(size=2048)
rolloff = es.RollOff(sampleRate = 44100)
array = []
for frame in es.FrameGenerator(audio, frameSize=2048, hopSize=1024, startFromZero=True):
    array.append(rolloff(spectrum(w(frame))))

fft_size = 2048
hop_length = 1024
sample_rate = 44100

loader = es.MonoLoader(filename='/Users/dan/Projects/SoundMaps/test/samples/tren_44k/tren_44k_0178.688549.wav')

audio = loader()
# Compute all features, aggregate only 'mean' and 'stdev' statistics for all low-level, rhythm and tonal frame features
me = es.Extractor(lowLevelFrameSize=fft_size, lowLevelHopSize=hop_length, sampleRate=sample_rate, dynamicsFrameSize=fft_size, dynamicsHopSize=hop_length)
features = me(audio)

features.descriptorNames()
features['lowLevel.mfcc']
