import numpy as np
import sys
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.svm import LinearSVC
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeRegressor
import util
from oscpy.client import OSCClient
import joblib

# testpath = sys.argv[1]
testpath = '/Users/dan/Library/Application Support/SuperCollider/tmp/PythonTest.txt'

# modelPath = sys.argv[2]
modelPath = '/Users/dan/Projects/Dev/SuperCollider/SoundMap/Models/PythonDLTest.sav'

# port = sys.argv[3]

# returnMessage = sys.argv[4]

model = joblib.load(modelPath)

test_data = util.parse_file(testpath)


mean = [ 0.44372567534447, 0.0611906722188, 0.025491639971733, 0.012392813339829, 0.0045111370272934, 0.0019815170671791, 0.00075251865200698, 0.0003490331582725, 16.613023757935, 0.12178305536509, 2346.4194335938, 6456.50390625 ]
std = [ 0.11130483448505, 0.023587036877871, 0.0095312921330333, 0.0046016578562558, 0.0016541614895687, 0.00067496317205951, 0.00022747801267542, 0.00010400835162727, 3.8015794754028, 0.012339330278337, 229.15620422363, 519.7138671875 ]
test_data = (test_data - mean) / std

index = model.predict(test_data)

osc = OSCClient("127.0.0.1", int(port))
osc.send_message(returnMessage.encode(), index[0].tolist())
