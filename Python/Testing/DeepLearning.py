import numpy as np
import sys
from sklearn.neural_network import MLPClassifier
import util
from oscpy.client import OSCClient
import joblib

filepath = '/Users/dan/Library/Application Support/SuperCollider/tmp/PythonDeepLearningTrain.txt'

testpath = '/Users/dan/Library/Application Support/SuperCollider/tmp/PythonTest.txt'

X_train = util.parse_file(filepath)

hidden_layer_sizes = [100, 100]

max_iter = 10000

mlp = MLPClassifier(hidden_layer_sizes=hidden_layer_sizes, max_iter=max_iter)
y = np.arange(len(X_train))

mean_on_train = X_train.mean(axis=0)
# compute the standard deviation of each feature on the training set
std_on_train = X_train.std(axis=0)
# subtract the mean, and scale by inverse standard deviation
# afterward, mean=0 and std=1
X_train_scaled = (X_train - mean_on_train) / std_on_train
# use THE SAME transformation (using training mean and std) on the test set

mlp.fit(X_train_scaled, y)

X_test = util.parse_file(testpath)
test_scaled = (X_test - mean_on_train) / std_on_train
index = mlp2.predict(test_scaled)

joblib.dump(mlp, '/Users/dan/Projects/Dev/SuperCollider/SoundMap/Models/PythonDLTest.sav')

mlp2 = joblib.load('/Users/dan/Projects/Dev/SuperCollider/SoundMap/Models/PythonDLTest.sav')

osc = OSCClient("127.0.0.1", int(port))
osc.send_message(returnMessage.encode(), [str(index[0]).encode()])
