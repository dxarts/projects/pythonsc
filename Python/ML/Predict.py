import numpy as np
import sys
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.svm import LinearSVC
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeRegressor
import mlUtil
import joblib
import argparse
import os
import sys
# sys.path.append(os.path.abspath("../Classes"))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../Classes")))
import Scaler

parser = argparse.ArgumentParser(description='Run a trained model on test data')

parser.add_argument('test_path', type=str,
                help='the path to the folder of test data')
parser.add_argument('model_path', type=str,
                help='the path to the trained model')
parser.add_argument('output_path', type=str,
                help='the path to save the prediction')
parser.add_argument('-s', '--scaler', type=str, default=None,
                help='the path to the scaler object')
parser.add_argument('-l', '--length', type=int, default=None,
                help='the minimum length of the data model')


args = parser.parse_args()

test_path = args.test_path
model_path = args.model_path
output_path = args.output_path
scaler = args.scaler
length = args.length

# FOR TESTING
# test_path = '/Users/dan/Projects/SoundMaps/tren_44k_0193.122041/Data/Data.npy'
# model_path = '/Users/dan/Projects/SoundMaps/test/Models/DL/test.sav'
# output_path =' /Users/dan/Library/Application Support/SuperCollider/tmp/PythonReturn.txt'
# scaler = '/Users/dan/Projects/SoundMaps/test/Scalers/MinMaxScaler.sav'
# length = 10

model = joblib.load(model_path)

test_data = np.load(test_path, allow_pickle=True)

scaler = joblib.load(scaler)

test_data = scaler.transform(test_data)

drop_zeroes = []
for data_inc,file_data in enumerate(test_data):
    drop_zeroes.append([])
    for feature_inc,feature in enumerate(file_data):
        drop_zeroes[data_inc].append([])
        for val in feature:
            if val != 0.0:
                drop_zeroes[data_inc][feature_inc].append(val)

drop_zeroes = np.array(drop_zeroes)

new_data = [feature[:length] for file_data in drop_zeroes for feature in file_data]
new_data = np.array(new_data)

new_data = new_data.reshape(test_data.shape[0], -1)

index = model.predict(new_data)

np.savetxt(output_path, index)
