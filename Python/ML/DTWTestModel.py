import numpy as np
import sys
from fastdtw import fastdtw
import mlUtil
import joblib
import argparse
from scipy.spatial.distance import euclidean
import os
from sklearn.model_selection import train_test_split

parser = argparse.ArgumentParser(description='Run a trained model on test data')

parser.add_argument('data_path', type=str,
                help='the path to the folder of scaled padded training data')
parser.add_argument('classification', type=str,
                help='the path to the classification data')
parser.add_argument('output_path', type=str,
                help='the path to save the model')
parser.add_argument('-l', '--hidden_layer_sizes', nargs='+', type=int, default=[10,10],
                help='a list of hidden layer sizes')
parser.add_argument('-m', '--max_iter', type=int, default=100,
                help='the maximum number of iterations')

args = parser.parse_args()

data_path = args.data_path
classification = args.classification
output_path = args.output_path
hidden_layer_sizes = args.hidden_layer_sizes
max_iter = args.max_iter

# data_path = '/Users/dan/Projects/SoundMaps/ESC50/Data/ScaledData.npy'
# classification = '/Users/dan/Library/Application Support/SuperCollider/tmp/PythonDLResult.npy'
# hidden_layer_sizes = [100, 100, 100]
# max_iter = 10000

X_train = np.load(data_path, allow_pickle=True)

X_train = np.array(X_train)

Y_train = np.load(classification, allow_pickle=True)

X_train, X_test, y_train, y_test = train_test_split(X_train, Y_train, stratify=Y_train, random_state=42)

pred = []
for data,res in zip(X_test, y_test):

    distances = []

    for fileData in X_train:
        dist = []
        for feature_num, feature in enumerate(fileData):
            distance, path = fastdtw(feature, data[feature_num], dist=euclidean)
            dist.append(distance)
        distances.append(sum(np.array(dist) ** 2) ** 0.5)

    index = np.argmin(distances)
    if y_train[index] == res:
        pred.append(1)
    else:
        pred.append(0)

print("Accuracy on test set: {:.3f}".format(sum(pred)/len(pred)))
