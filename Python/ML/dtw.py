from fastdtw import fastdtw
from scipy.spatial.distance import euclidean
import numpy as np
import argparse
import os
import sys
import mlUtil

parser = argparse.ArgumentParser(description="Calculate the DTW distance between points")

parser.add_argument('data_path', type=str,
                help='the path to the sound map data file')
parser.add_argument('test_data_path', type=str,
                help='the path to the test data file')
parser.add_argument('output_path', type=str,
                help='the path to store the neareast index')

args = parser.parse_args()
data_path = args.data_path
test_data_path = args.test_data_path
output_path = args.output_path

# data_path = '/Users/dan/Projects/SoundMaps/test/pythonAnalysisData/Data.npy'
# test_data_path = "/Users/dan/Library/Application Support/SuperCollider/tmp/PythonTest.txt"

data = np.load(data_path, allow_pickle=True)

test_data = np.load(test_data_path, allow_pickle=True)[0]

distances = []

for fileData in data:
    dist = []
    for feature_num, feature in enumerate(fileData):
        distance, path = fastdtw(feature, test_data[feature_num], dist=euclidean)
        dist.append(distance)
    distances.append(sum(np.array(dist) ** 2) ** 0.5)

index = np.argmin(distances)
np.savetxt(output_path, [index])
