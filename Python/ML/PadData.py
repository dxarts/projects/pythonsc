import numpy as np
import sys
from sklearn.neural_network import MLPClassifier
import mlUtil
import joblib
import argparse
import os

parser = argparse.ArgumentParser(description='Pad the data')

parser.add_argument('data_path', type=str,
                help='the path to the folder of train data')
parser.add_argument('classification', type=str,
                help='the path to the classification data')
parser.add_argument('output_path', type=str,
                help='the path to save the model')
parser.add_argument('-p', '--length_path', type=str,
                help='the path to save the pad length')
parser.add_argument('-s', '--scaler_path', type=str,
                help='the path to the scaler')
parser.add_argument('-l', '--hidden_layer_sizes', nargs='+', type=int, default=[10,10],
                help='a list of hidden layer sizes')
parser.add_argument('-m', '--max_iter', type=int, default=100,
                help='the maximum number of iterations')

args = parser.parse_args()

data_path = args.data_path
classification = args.classification
output_path = args.output_path
scaler_path = args.scaler_path
hidden_layer_sizes = args.hidden_layer_sizes
length_path = args.length_path
max_iter = args.max_iter

X_train = mlUtil.parse_file(data_path)

Y_train = mlUtil.parse_file(classification)

mlp = MLPClassifier(hidden_layer_sizes=hidden_layer_sizes, max_iter=max_iter)

X_train, max = pad_data.pad_data(X_train, 0)

X_train = np.array(X_train)

X_train = X_train.reshape(X_train.shape[0], -1)

X_train = scaler.transform(X_train)

mlp.fit(X_train, Y_train)

joblib.dump(mlp, output_path)
np.savetxt(os.path.join(length_path, "length.csv"), [max])
