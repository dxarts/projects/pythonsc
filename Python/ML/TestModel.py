import numpy as np
import sys
from sklearn.neural_network import MLPClassifier
import mlUtil
import joblib
import argparse
import os
from sklearn.model_selection import train_test_split

parser = argparse.ArgumentParser(description='Run a trained model on test data')

parser.add_argument('data_path', type=str,
                help='the path to the folder of scaled padded training data')
parser.add_argument('classification', type=str,
                help='the path to the classification data')
parser.add_argument('output_path', type=str,
                help='the path to save the model')
parser.add_argument('-l', '--hidden_layer_sizes', nargs='+', type=int, default=[10,10],
                help='a list of hidden layer sizes')
parser.add_argument('-m', '--max_iter', type=int, default=100,
                help='the maximum number of iterations')

args = parser.parse_args()

data_path = args.data_path
classification = args.classification
output_path = args.output_path
hidden_layer_sizes = args.hidden_layer_sizes
max_iter = args.max_iter

# data_path = '/Users/dan/Projects/SoundMaps/test/ScaledData'
# classification = args.classification
# output_path = args.output_path
# hidden_layer_sizes = args.hidden_layer_sizes
# max_iter = args.max_iter

X_train = np.load(data_path, allow_pickle=True)

X_train, length = mlUtil.pad_data(X_train, 0)
length_path = os.path.abspath(os.path.join(data_path, os.path.pardir, os.path.pardir))
np.savetxt(os.path.join(length_path, "length.csv"), [length])
X_train = np.array(X_train)
X_train = X_train.reshape(X_train.shape[0], -1)
Y_train = np.load(classification, allow_pickle=True)

X_train, X_test, y_train, y_test = train_test_split(X_train, Y_train, stratify=Y_train, random_state=42)

mlp = MLPClassifier(hidden_layer_sizes=hidden_layer_sizes, max_iter=max_iter)

mlp.fit(X_train, Y_train)

print("Accuracy on training set: {:.3f}".format( mlp.score(X_train, Y_train)))
print("Accuracy on test set: {:.3f}".format(mlp.score(X_test, y_test)))

joblib.dump(mlp, output_path)
