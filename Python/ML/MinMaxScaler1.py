import numpy as np
import mlUtil
import joblib
import argparse
import os
from sklearn.preprocessing import MinMaxScaler

parser = argparse.ArgumentParser(description='Run a trained model on test data')

parser.add_argument('data_path', type=str,
                help='the path to the folder of train data')
parser.add_argument('output_path', type=str,
                help='the path to save the model')
parser.add_argument('data_output_path', type=str,
                help='the path to save the data')

args = parser.parse_args()
data = np.load(args.data_path, allow_pickle=True)
output_path = args.output_path
data_output_path = args.data_output_path

# data = np.load('/Users/dan/Projects/SoundMaps/test/Data/Data.npy', allow_pickle=True)
# output_path = '/Users/dan/Projects/SoundMaps/test/Scalers/MinMaxScaler.sav'
# data_output_path = '/Users/dan/Projects/SoundMaps/test/Data/ScaledData.npy'

scaler = MinMaxScaler()

# transpose the data so it is [feature, file, feature_data]
if len(data.shape) == 3:
    transposed_data = np.transpose(data, axes=[1,0,2])
else:
    transposed_data = np.transpose(data, axes=[1,0])

# reshape the data to [feature, allData featureData]
reshaped_data = []
for inc,feature in enumerate(transposed_data):
    reshaped_data.append([])
    for fileData in feature:
        for data_point in fileData:
            reshaped_data[inc].append(data_point)

# transpose so the data is [allData features, feature]
reshaped_data = np.array(reshaped_data)
reshaped_data = np.transpose(reshaped_data)
# scale the data
scaled_data
X_std = (X - X.min(axis=0)) / (X.max(axis=0) - X.min(axis=0))
X_scaled = X_std * (max - min) + min
scaler.fit(reshaped_data)

joblib.dump(scaler, output_path)
reshaped_data = scaler.transform(reshaped_data)

# transpose back to [feature, allData features]
reshaped_data = np.transpose(reshaped_data)

# reshape back to [feature, fileData, featureData]
scaled_data = []
sizes = [0] + [len(dat[0]) for dat in data]
indices = []
for inc in range(len(sizes)):
    indices.append(sum(sizes[:inc+1]))

for inc,feature in enumerate(reshaped_data):
    scaled_data.append([])
    for file_num in range(data.shape[0]):
        scaled_data[inc].append(feature[indices[file_num]:indices[file_num+1]].tolist())


# transpose back to [fileData, features, featureData]
if len(data.shape) == 3:
    scaled_data = np.transpose(np.array(scaled_data), axes=[1,0,2])
else:
    scaled_data = np.transpose(np.array(scaled_data, dtype=object), axes=[1,0])

np.save(data_output_path, scaled_data)
