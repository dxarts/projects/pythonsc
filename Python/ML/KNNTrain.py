import numpy as np
import sys
from sklearn.neighbors import KNeighborsClassifier
import mlUtil
import joblib
import argparse
import os

parser = argparse.ArgumentParser(description='Train KNN Model')

parser.add_argument('data_path', type=str,
                help='the path to the folder of train data')
parser.add_argument('classification', type=str,
                help='the path to the classification data')
parser.add_argument('output_path', type=str,
                help='the path to save the model')
parser.add_argument('-n', '--n_neighbors', type=int, default=1,
                help='the number of nearest neighbors')

args = parser.parse_args()

data_path = args.data_path
classification = args.classification
output_path = args.output_path
n = args.n_neighbors

X_train = np.load(data_path, allow_pickle=True)

drop_zeroes = []
for data_inc,file_data in enumerate(X_train):
    drop_zeroes.append([])
    for feature_inc,feature in enumerate(file_data):
        drop_zeroes[data_inc].append([])
        for val in feature:
            if val != 0.0:
                drop_zeroes[data_inc][feature_inc].append(val)

drop_zeroes = np.array(drop_zeroes)
min = np.min([len(file_data[0]) for file_data in drop_zeroes])
length_path = os.path.abspath(os.path.join(data_path, os.path.pardir, os.path.pardir))
np.savetxt(os.path.join(length_path, "length.csv"), [min])
new_data = [feature[:min] for file_data in drop_zeroes for feature in file_data]
new_data = np.array(new_data)
new_data = new_data.reshape(X_train.shape[0], X_train.shape[1], min)
X_train = new_data.reshape(new_data.shape[0], -1)

knn = KNeighborsClassifier(n_neighbors=n)

y = np.load(classification, allow_pickle=True)
print(classification)
knn.fit(X_train, y)

joblib.dump(knn, output_path)
