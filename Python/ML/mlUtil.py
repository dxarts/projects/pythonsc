import numpy as np
import sys

def parse_file(filepath):
    data = []
    file = open(filepath)
    for line in file:
        event = eval(line)
        data.append(event)
    return data

def dtw(s, t, window=0):
    n, m = len(s), len(t)
    w = np.max([window, abs(n-m)])
    dtw_matrix = np.zeros((n+1, m+1))

    for i in range(n+1):
        for j in range(m+1):
            dtw_matrix[i, j] = np.inf
    dtw_matrix[0, 0] = 0

    for i in range(1, n+1):
        for j in range(np.max([1, i-w]), np.min([m, i+w])+1):
            dtw_matrix[i, j] = 0

    for i in range(1, n+1):
        for j in range(np.max([1, i-w]), np.min([m, i+w])+1):
            cost = abs(s[i-1] - t[j-1])
            # take last min from a square box
            last_min = np.min([dtw_matrix[i-1, j], dtw_matrix[i, j-1], dtw_matrix[i-1, j-1]])
            dtw_matrix[i, j] = cost + last_min
    return dtw_matrix[-1][-1]

def pad_data(data, length):

    if length <= 0:
        length = np.max([len(dat[0]) for dat in data])

    result_data = []
    for file_data in data:
        data_array =  []
        for feature in file_data:
            if len(feature) < length:
                array = np.zeros(length) + np.min(feature)
                for inc, data_point in enumerate(feature):
                    array[inc] = data_point
            else:
                array = feature[:length]
            if not type(array) is list:
                array = array.tolist()
            data_array.append(array)
        result_data.append(data_array)
    return [result_data, length]
