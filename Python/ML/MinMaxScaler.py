import numpy as np
import mlUtil
import joblib
import argparse
import os
import sys
# sys.path.append(os.path.abspath("../Classes"))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../Classes")))
import Scaler

parser = argparse.ArgumentParser(description='Run a trained model on test data')

parser.add_argument('data_path', type=str,
                help='the path to the folder of train data')
parser.add_argument('output_path', type=str,
                help='the path to save the model')
parser.add_argument('data_output_path', type=str,
                help='the path to save the data')
parser.add_argument('-a', '--axes', nargs='+', type=int, default = None,
                help='the axes of descriptors')
parser.add_argument('-m', '--min', type=float, default=0.0,
                help='the min of the scaling')
parser.add_argument('-x', '--max', type=float, default=1.0,
                help='the max of the scaling')

args = parser.parse_args()
data = np.load(args.data_path, allow_pickle=True)
output_path = args.output_path
data_output_path = args.data_output_path
axes = args.axes
min = args.min
max = args.max

# data = np.load('/Users/dan/Projects/SoundMaps/test/Data/Data.npy', allow_pickle=True)
# output_path = '/Users/dan/Projects/SoundMaps/test/Scalers/MinMaxScaler.sav'
# data_output_path = '/Users/dan/Projects/SoundMaps/test/Data/ScaledData.npy'
# min = 0.0
# max = 1.0
# axes = None

scaler = Scaler.Scaler()

scaled_data = scaler.transform(data, min, max, axes=axes)

joblib.dump(scaler, output_path)

np.save(data_output_path, scaled_data)
