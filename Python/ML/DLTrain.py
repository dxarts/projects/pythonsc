import numpy as np
import sys
from sklearn.neural_network import MLPClassifier
import mlUtil
import joblib
import argparse
import os

parser = argparse.ArgumentParser(description='Train a DL Model')

parser.add_argument('data_path', type=str,
                help='the path to the folder of scaled padded training data')
parser.add_argument('classification', type=str,
                help='the path to the classification data')
parser.add_argument('output_path', type=str,
                help='the path to save the model')
parser.add_argument('-l', '--hidden_layer_sizes', nargs='+', type=int, default=[10,10],
                help='a list of hidden layer sizes')
parser.add_argument('-m', '--max_iter', type=int, default=100,
                help='the maximum number of iterations')

args = parser.parse_args()

data_path = args.data_path
classification = args.classification
output_path = args.output_path
hidden_layer_sizes = args.hidden_layer_sizes
max_iter = args.max_iter

data_path = '/Users/dan/Projects/SoundMaps/test/Data/ScaledData.npy'
classification = '/Users/dan/Library/Application Support/SuperCollider/tmp/PythonDLResult.npy'
# output_path = args.output_path
hidden_layer_sizes = [100, 100, 100]
max_iter = 1000

X_train = np.load(data_path, allow_pickle=True)

drop_zeroes = []
for data_inc,file_data in enumerate(X_train):
    drop_zeroes.append([])
    for feature_inc,feature in enumerate(file_data):
        drop_zeroes[data_inc].append([])
        for val in feature:
            if val != 0.0:
                drop_zeroes[data_inc][feature_inc].append(val)

drop_zeroes = np.array(drop_zeroes)
min = np.min([len(file_data[0]) for file_data in drop_zeroes])
length_path = os.path.abspath(os.path.join(data_path, os.path.pardir, os.path.pardir))
np.savetxt(os.path.join(length_path, "length.csv"), [min])
new_data = [feature[:min] for file_data in drop_zeroes for feature in file_data]
new_data = np.array(new_data)
new_data = new_data.reshape(X_train.shape[0], X_train.shape[1], min)
X_train = new_data.reshape(new_data.shape[0], -1)

Y_train = np.load(classification, allow_pickle=True)

mlp = MLPClassifier(hidden_layer_sizes=hidden_layer_sizes, max_iter=max_iter, solver='lbfgs')

mlp.fit(X_train, Y_train)

joblib.dump(mlp, output_path)
