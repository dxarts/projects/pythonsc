import librosa
import numpy as np
import os
from analysisUtil import librosa_arg_parser

parser = librosa_arg_parser.librosa_arg_parser('Duration Using Librosa')

args = parser.parse_args()
sound_path = args.sound_path
output_path = args.output_path


# FOR TESTING
# sound_folder = '/Users/dan/Projects/SoundMaps/test2/samples'
# output_path = '/Users/dan/Projects/SoundMaps/test2/mags.npy'
# files_path = '/Users/dan/Projects/SoundMaps/test2/Files.npy'


duration = np.array(librosa.get_duration(filename=sound_path))
np.save(output_path, duration)
