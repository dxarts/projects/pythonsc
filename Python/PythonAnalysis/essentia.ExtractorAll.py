import sys
import numpy as np
import os
from analysisUtil import analysis_arg_parser
import essentia
import essentia.standard as es

parser = analysis_arg_parser.analysis_arg_parser('Sound File Analysis Using Essentia')

parser.add_argument('feature_path', type=str,
                help='the path to save the features list')


args = parser.parse_args()
sound_folder = args.sound_folder
files_path = args.files_path
sample_rate = args.sample_rate
fft_size = args.fft_size
hop_length = args.hop_length
output_path = args.output_path
feature_path = args.feature_path

# FOR TESTING
# sound_folder = '/Users/dan/Projects/SoundMaps/test/samples'
# files_path = '/Users/dan/Projects/SoundMaps/test/Files.npy'
# sample_rate = 44100
# fft_size = 2048
# hop_length = 1024
# analysis_types = ['mfcc', 'spectral_energy', 'spectral_centroid', 'spectral_rolloff', 'spectral_flatness_db']
# output_path = '/Users/dan/Projects/SoundMaps/test/Data/Data.npy'

files_list = [os.path.join(sound_folder, file) for file in np.load(files_path, allow_pickle=True)]
data = []
feature = []

# Compute all features, aggregate only 'mean' and 'stdev' statistics for all low-level, rhythm and tonal frame features
extractor = es.Extractor(lowLevelFrameSize=fft_size, lowLevelHopSize=hop_length, sampleRate=sample_rate, dynamicsFrameSize=fft_size, dynamicsHopSize=hop_length)

used_features = []
for filename in files_list:
    loader = es.MonoLoader(filename=filename)
    audio = loader()
    features = extractor(audio)
    file_data = []
    for feature in features.descriptorNames():
        if "lowLevel" in feature and not "average_loudness" in feature:
            used_features.append(feature)
            if any([analysis_type in feature for analysis_type in ['mfcc', 'barkbands', 'sccoeffs', 'scvalleys']]) and not any([analysis_type in feature for analysis_type in ['kurtosis', 'skewness', 'spread']]):
                feature_data = features[feature].transpose();
                for band in feature_data:
                    file_data.append(band.tolist())
            else:
                file_data.append(features[feature].tolist())
    data.append(file_data)

np.save(output_path, np.array(data))
np.save(feature_path, np.array(used_features))
