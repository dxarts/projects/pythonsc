import numpy as np
import os
from util import analysis_arg_parser
from util import parse_folder
import essentia
import essentia.standard as es

parser = analysis_arg_parser.analysis_arg_parser('Sound File Analysis Using Essentia')

parser.add_argument('-a', '--analysis_types', nargs=+, default=['fft', 'spectral_energy', 'spectral_centroid', 'spectral_rolloff', 'spectral_flatness_db'],
                    help='the analysis types')

args = parser.parse_args()
sound_path = args.sound_path
sample_rate = args.sample_rate
fft_size = args.fft_size
hop_length = args.hop_length
analysis_types = args.analysis_types
output_path = args.output_path

# FOR TESTING
# sound_folder = '/Users/dan/Projects/SoundMaps/test/samples'
# sample_rate = 44100
# fft_size = 2048
# hop_length = 1024
# roll_percent = 0.85
# analysis_types = ['fft', 'spectral_energy', 'spectral_centroid', 'spectral_rolloff', 'spectral_flatness_db']
# output_path = '/Users/dan/Projects/SoundMaps/test/pythonAnalysisData/Data.npy'
# files_path = '/Users/dan/Projects/SoundMaps/test/Files.npy'
# save_files = True

files_list = parse_folder.parse_folder(sound_folder, files_path, save_files)
data = []
feature = []


# Compute all features, aggregate only 'mean' and 'stdev' statistics for all low-level, rhythm and tonal frame features
me = es.Extractor(lowLevelFrameSize=fft_size, lowLevelHopSize=hop_length, sampleRate=sample_rate, dynamicsFrameSize=fft_size, dynamicsHopSize=hop_length)

features = me(sound_path)
for analysis_type in analysis_types:
    data.append(features["lowLevel." + analysis_type])

data = np.array(data)

np.save(output_path, data)
