import numpy as np
from analysisUtil import analysis_arg_parser
import essentia
import essentia.standard as es

parser = analysis_arg_parser.analysis_arg_parser('FFT Sound File Analysis Using Essentia')

args = parser.parse_args()
sound_path = args.sound_path
sample_rate = args.sample_rate
fft_size = args.fft_size
hop_length = args.hop_length
output_path = args.output_path


# FOR TESTING
# sound_folder = '/Users/dan/Projects/SoundMaps/test/samples'
# sample_rate = 44100
# fft_size = 2048
# hop_length = 1024
# output_path = '/Users/dan/Projects/SoundMaps/test/LibrosaData/fft.npy'
# files_path = '/Users/dan/Projects/SoundMaps/test/Files.npy'

w = es.Windowing(type = 'hann', size=fft_size)
spectrum = es.Spectrum(size=fft_size)

loader = es.MonoLoader(filename=sound_path)

audio = loader()
magnitude = []
for frame in es.FrameGenerator(audio, frameSize=fft_size, hopSize=hop_length, startFromZero=True):
    magnitude.append(spectrum(w(frame)).tolist()[1:])
magnitude = np.array(magnitude).transpose()

np.save(output_path, magnitude)
