import argparse

def analysis_arg_parser(description):

    parser = argparse.ArgumentParser(description=description)

    parser.add_argument('sound_path', type=str,
                    help='the path to the sound file')
    parser.add_argument('output_path', type=str,
                    help='the path to store the data')
    parser.add_argument('-s', '--sample_rate', type=int, default=48000,
                    help='the sample rate at which to run the analysis (default: 2048)')
    parser.add_argument('-f', '--fft_size', type=int, default=2048,
                    help='the size of the FFT (default: 2048)')
    parser.add_argument('-l', '--hop_length', type=int, default=1024,
                    help='the hop size of the FFT in samples (default: 1024)')
    return parser
