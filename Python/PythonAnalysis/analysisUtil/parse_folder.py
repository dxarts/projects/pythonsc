import os
import glob
import numpy as np

def parse_folder(folder_path, files_path=None, save_files=False):
    if os.path.isfile(folder_path):
        files_list = [folder_path]
        rel_list = [folder_path]
    else:
        files_list = []
        rel_list = []
        for filename in glob.iglob(folder_path + '/**/*.wav', recursive=True):
            files_list.append(filename)
            rel_list.append(os.path.relpath(filename, folder_path))
        for filename in glob.iglob(folder_path + '/**/*.WAV', recursive=True):
            files_list.append(filename)
            rel_list.append(os.path.relpath(filename, folder_path))
    if files_path:
        if os.path.exists(files_path) and not save_files and not os.path.isfile(folder_path):
            files_list = [os.path.join(folder_path, filename) for filename in np.load(files_path)]
        elif not os.path.exists(files_path) or save_files:
            np.save(files_path, rel_list)
            np.savetxt(files_path.replace('npy', 'csv'), rel_list, delimiter=',', fmt="%s")
    return files_list
