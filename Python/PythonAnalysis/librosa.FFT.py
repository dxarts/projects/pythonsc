import librosa
import numpy as np
import os
from analysisUtil import analysis_arg_parser

parser = analysis_arg_parser.analysis_arg_parser('FFT Sound File Analysis Using Librosa')

args = parser.parse_args()
sound_path = args.sound_path
sample_rate = args.sample_rate
fft_size = args.fft_size
hop_length = args.hop_length
output_path = args.output_path

# FOR TESTING
# sound_folder = '/Users/dan/Projects/SoundMaps/test/samples'
# sample_rate = 44100
# fft_size = 2048
# hop_length = 1024
# average = 8
# output_path = '/Users/dan/Projects/SoundMaps/test/LibrosaData/fft.npy'
# files_path = '/Users/dan/Projects/SoundMaps/test/Files.npy'


y, sr = librosa.load(sound_path, sr=sample_rate)
D = librosa.stft(y, n_fft=fft_size, hop_length=hop_length)
magnitude, phase = librosa.magphase(D)
    
np.save(output_path, magnitude)
