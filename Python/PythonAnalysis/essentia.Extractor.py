import numpy as np
from analysisUtil import analysis_arg_parser
import essentia
import essentia.standard as es

parser = analysis_arg_parser.analysis_arg_parser('Sound File Analysis Using Essentia')

parser.add_argument('-a', '--analysis_types', nargs='+', type=str, default=['mfcc', 'spectral_energy', 'spectral_centroid', 'spectral_rolloff', 'spectral_flatness_db'],
                    help='the analysis types')


args = parser.parse_args()
sound_path = args.sound_path
sample_rate = args.sample_rate
fft_size = args.fft_size
hop_length = args.hop_length
analysis_types = args.analysis_types
output_path = args.output_path

# FOR TESTING
# sound_folder = '/Users/dan/Projects/SoundMaps/test/samples'
# files_path = '/Users/dan/Projects/SoundMaps/test/Files.npy'
# sample_rate = 44100
# fft_size = 2048
# hop_length = 1024
# analysis_types = ['mfcc', 'spectral_energy', 'spectral_centroid', 'spectral_rolloff', 'spectral_flatness_db']
# output_path = '/Users/dan/Projects/SoundMaps/test/Data/Data.npy'

data = []
feature = []

# Compute all features, aggregate only 'mean' and 'stdev' statistics for all low-level, rhythm and tonal frame features
extractor = es.Extractor(lowLevelFrameSize=fft_size, lowLevelHopSize=hop_length, sampleRate=sample_rate, dynamicsFrameSize=fft_size, dynamicsHopSize=hop_length)

loader = es.MonoLoader(filename=sound_path)
audio = loader()
features = extractor(audio)
for analysis_type in analysis_types:
    if analysis_type == 'mfcc':
        mfcc = features['lowLevel.mfcc'].transpose();
        for band in mfcc:
            data.append(band.tolist())
    else:
        data.append(features["lowLevel." + analysis_type].tolist())

np.save(output_path, np.array(data))
