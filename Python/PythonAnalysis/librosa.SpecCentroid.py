import librosa
import numpy as np
import os
from analysisUtil import analysis_arg_parser

parser = analysis_arg_parser.analysis_arg_parser('Spectral Centroid Sound File Analysis Using Librosa')

args = parser.parse_args()
sound_path = args.sound_path
sample_rate = args.sample_rate
fft_size = args.fft_size
hop_length = args.hop_length
output_path = args.output_path

# FOR TESTING
# sound_folder = '/Users/dan/Projects/SoundMaps/test/samples'
# sample_rate = 48000
# fft_size = 2048
# hop_length = 1024
# output_path = '/Users/dan/Projects/SoundMaps/test/sc.npy'
# files_path = '/Users/dan/Projects/SoundMaps/test/Files.npy'

y, sr = librosa.load(sound_path, sr=sample_rate)
sc = librosa.feature.spectral_centroid(y, sr=sr, n_fft=fft_size, hop_length=hop_length)[0]

np.save(output_path, sc)

#scresult = np.load('/Users/dan/Projects/Dev/SuperCollider/Python/Python/Librosa/Data/FFT.npy', allow_pickle=True)
