import librosa
import numpy as np
import os
from analysisUtil import analysis_arg_parser
from analysisUtil import parse_folder

parser = analysis_arg_parser.analysis_arg_parser('Spectral Percentile (Rolloff) Sound File Analysis Using Librosa')

parser.add_argument('-p', '--percent', type=float, default=0.85,
                    help='the roll-off percentage (default: 0.85)')

args = parser.parse_args()
sound_path = args.sound_path
sample_rate = args.sample_rate
fft_size = args.fft_size
hop_length = args.hop_length
roll_percent = args.percent
output_path = args.output_path


# FOR TESTING
# sound_folder = '/Users/dan/Projects/SoundMaps/test2/samples'
# sample_rate = 48000
# fft_size = 2048
# hop_length = 1024
# roll_percent = 0.9
# output_path = '/Users/dan/Projects/SoundMaps/test2/mags.npy'
# files_path = '/Users/dan/Projects/SoundMaps/test2/Files.npy'

y, sr = librosa.load(sound_path, sr=sample_rate)
sp = librosa.feature.spectral_rolloff(y, n_fft=fft_size, hop_length=hop_length, roll_percent=roll_percent)[0]
np.save(output_path, sp)

#scresult = np.load('/Users/dan/Projects/Dev/SuperCollider/Python/Python/Librosa/Data/FFT.npy', allow_pickle=True)
