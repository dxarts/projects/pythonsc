import essentia
import essentia.standard as es
import numpy as np
from analysisUtil import analysis_arg_parser


parser = analysis_arg_parser.analysis_arg_parser('Spectral Rolloff Using Essentia')

parser.add_argument('-p', '--percent', type=float, default=0.85,
                    help='the roll-off percentage (default: 0.85)')

args = parser.parse_args()
sound_path = args.sound_path
sample_rate = args.sample_rate
fft_size = args.fft_size
hop_length = args.hop_length
roll_percent = args.percent
output_path = args.output_path
w = es.Windowing(type = 'hann')

# FOR TESTING
# sound_folder = '/Users/dan/Projects/SoundMaps/test/samples'
# sample_rate = 44100
# fft_size = 2048
# hop_length = 1024
# roll_percent = 0.85
# output_path = '/Users/dan/Projects/SoundMaps/test/LibrosaData/fft.npy'
# files_path = '/Users/dan/Projects/SoundMaps/test/Files.npy'

spec_pcile = []

spectrum = es.Spectrum(size=fft_size)
rolloff = es.RollOff(cutoff=roll_percent, sampleRate=sample_rate)

loader = es.MonoLoader(filename=sound_path)

audio = loader()

for frame in es.FrameGenerator(audio, frameSize=fft_size, hopSize=hop_length, startFromZero=True):
    spec_pcile.append(rolloff(spectrum(w(frame))))

spec_pcile = np.array(spec_pcile)

np.save(output_path, spec_pcile)
