import argparse
import numpy as np
import os

parser = argparse.ArgumentParser(description='Consolidate Librosa Analysis Data')

parser.add_argument('data_folder', type=str,
                help='the path to the folder of data')
parser.add_argument('output_path', type=str,
                help='the path to the folder of data')
parser.add_argument('-a', '--analysis_types', nargs='+', type=str, required=True,
                help='the types of analysis to load')

args = parser.parse_args()
data_folder = args.data_folder
analysis_types = args.analysis_types
output_path = args.output_path
# data_folder = '/Users/dan/Projects/SoundMaps/test/pythonAnalysisData/'
# analysis_types = ['fft', 'loudness', 'specFlatness', 'specCentroid', 'specPcile']

data = []

for filename in analysis_types:
    filename = filename + ".npy"
    if filename == "fft.npy":
        fft = np.load(os.path.join(data_folder, filename), allow_pickle=True)
        for i in range(fft.shape[1]):
            data.append(fft[:,i].tolist())
    else:
        data.append(np.load(os.path.join(data_folder, filename), allow_pickle=True).tolist())

# transpose the data
transposed_data = []

for i in range(len(data[0])):
    transposed_data.append([])

for feature in data:
    for count, file in enumerate(feature):
        transposed_data[count].append(file)

np.save(output_path, np.array(transposed_data, dtype=object))
