import matplotlib.pyplot as plt
import numpy as np
import sys
from matplotlib.gridspec import GridSpec
from mpl_toolkits import mplot3d

list = []

filepath = sys.argv[1]

title = sys.argv[2]

labels = eval(sys.argv[3])

file = open(filepath)

for line in file:
    event = eval(line)
    list.append(event)

ax = plt.axes(projection='3d')

ax.scatter3D(list[0], list[1], list[2], c=np.arange(0.0, 1.0, 1.0/len(list[0])))

ax.set_title(title)
ax.set_xlabel(labels[0])
ax.set_ylabel(labels[1])
ax.set_zlabel(labels[2])
plt.show()
