import matplotlib.pyplot as plt
import numpy as np
import sys
from matplotlib import colors
import util
# from matplotlib.ticker import PercentFormatter

n_bins = 127

filepath = sys.argv[1]

title = sys.argv[2]

xLabels = eval(sys.argv[3])

list = util.parse_file(filepath)

if len(list) > 1:

    fig, axs = plt.subplots(1, len(list), tight_layout=True)

    for count, track in enumerate(list):

        N, bins, patches = axs[count].hist(track, bins=n_bins, label=str(xLabels[count]))

        # We'll color code by height, but you could use any scalar
        fracs = N / np.max(N)

        # we need to normalize the data to 0..1 for the full range of the colormap
        norm = colors.Normalize(np.min(fracs), np.max(fracs))

        # Now, we'll loop through our objects and set the color of each accordingly
        for thisfrac, thispatch in zip(fracs, patches):
            color = plt.cm.viridis(norm(thisfrac))
            thispatch.set_facecolor(color)
        axs[count].legend(loc="upper right")


    axs[0].set_ylabel('Number of Occurrences')
    axs[int(len(list)/2)].set_xlabel(title)


else:
    track = track.transpose()
    N, bins, patches = plt.hist(track, bins=n_bins, label=str(xLabels[0]))

    # We'll color code by height, but you could use any scalar
    fracs = N / np.max(N)

    # we need to normalize the data to 0..1 for the full range of the colormap
    norm = colors.Normalize(np.min(fracs), np.max(fracs))

    # Now, we'll loop through our objects and set the color of each accordingly
    for thisfrac, thispatch in zip(fracs, patches):
        color = plt.cm.viridis(norm(thisfrac))
        thispatch.set_facecolor(color)

    plt.legend(loc="upper right")

    plt.xlabel(title)

    plt.ylabel('Number of Occurrences')

plt.show()
