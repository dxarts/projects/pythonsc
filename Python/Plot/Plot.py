import matplotlib.pyplot as plt
import numpy as np
import sys
from matplotlib.gridspec import GridSpec
import util

filepath = sys.argv[1]

title = sys.argv[2]

xLabel = sys.argv[3]

yLabels = eval(sys.argv[4])

list = util.parse_file(filepath)

numSubPlots = len(list)
largestSubPlot = np.max([x[0][-1] for x in list])

fig, axs = plt.subplots(numSubPlots)
if numSubPlots == 1:
    axs = [axs]
for count, track in enumerate(list):
    axs[count].plot(track[0], track[1])
    axs[count].set_ylabel(yLabels[count])
    axs[count].set_xlim([0, largestSubPlot])
axs[0].set_title(title)
axs[-1].set_xlabel(xLabel)
plt.show()
