import numpy as np
from pandas.core.common import flatten

class Scaler:
    def __init__(self):
        '''MinMaxScaler'''
        self.data_min = []
        self.data_max = []
        self.axes = None

    def transform(self, data, min = 0, max = 1, axes = None):

        if not self.axes or axes:
            axes = axes or np.ones(len(data[0]), dtype=np.int32)

            indices = [0]
            for inc in range(len(axes)):
                indices.append(sum(axes[:inc+1]))

            ax = list(range(data.shape[1]))
            ax1 = []
            for index in range(len(indices[1:])):
                ax1.append(ax[indices[index]:indices[index+1]])
            self.axes = ax1
        load_max_min = bool(self.data_max and self.data_min)

        # transpose the data so it is [feature, file, feature_data]
        if len(data.shape) == 3:
            transposed_data = np.transpose(data, axes=[1,0,2])
        else:
            transposed_data = np.transpose(data, axes=[1,0])

        # scale the data
        recombined_data = [transposed_data[axis] for axis in self.axes]

        scaled_data = []
        for inc,dat in enumerate(recombined_data):
            flat_data = np.array(list(flatten(dat.tolist())))

            # if we have previous max and min data, use it
            if load_max_min:
                data_min = self.data_min[inc]
                data_max = self.data_max[inc]
            # otherwise calculate it and save it for later
            else:
                data_min = flat_data.min()
                data_max = flat_data.max()
                self.data_min.append(data_min)
                self.data_max.append(data_max)

            X_std = (flat_data - data_min) / (data_max - data_min)
            scaled_data.append(X_std * (max - min) + min)

        flat_data = np.array(list(flatten(scaled_data))).reshape(transposed_data.shape[0], -1)

        # reshape back to [feature, fileData, featureData]
        scaled_data = []
        sizes = [0] + [len(dat[0]) for dat in data]
        indices = []
        for inc in range(len(sizes)):
            indices.append(sum(sizes[:inc+1]))

        for inc,feature in enumerate(flat_data):
            scaled_data.append([])
            for file_num in range(data.shape[0]):
                scaled_data[inc].append(feature[indices[file_num]:indices[file_num+1]].tolist())

        # transpose back to [fileData, features, featureData]
        if len(data.shape) == 3:
            scaled_data = np.transpose(np.array(scaled_data), axes=[1,0,2])
        else:
            scaled_data = np.transpose(np.array(scaled_data, dtype=object), axes=[1,0])

        return scaled_data
