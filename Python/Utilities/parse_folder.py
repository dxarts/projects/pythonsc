import os
import glob
import numpy as np
import argparse
import csv

parser = argparse.ArgumentParser(description='Parse a folder and return a numpy array file')

parser.add_argument('files_path', type=str,
                help='the path to the files folder')
parser.add_argument('save_path', type=str,
                help='the path to the save the files array')

args = parser.parse_args()

files_path = args.files_path
save_path = args.save_path

if os.path.isfile(files_path):
    rel_list = [files_path]
else:
    rel_list = []
    for filename in glob.iglob(files_path + '/**/*.wav', recursive=True):
        rel_list.append(os.path.relpath(filename, files_path))
    for filename in glob.iglob(files_path + '/**/*.WAV', recursive=True):
        rel_list.append(os.path.relpath(filename, files_path))

np.save(save_path, rel_list)
np.savetxt(save_path.replace('npy', 'csv'), rel_list, delimiter=',', fmt="%s")
