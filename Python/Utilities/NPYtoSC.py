import numpy as np
import os
import argparse
import csv
import pandas as pd

parser = argparse.ArgumentParser(description='Send Numpy Data to SuperCollider')

parser.add_argument('numpy_file', type=str,
                help='the path to the numpy data file')

parser.parse_args()

numpy_file = args.numpy_file

# FOR TESTING
# numpy_file = '/Users/dan/Projects/SoundMaps/test2/Data/Data.npy'

data = np.load(numpy_file, allow_pickle=True).tolist()

df = pd.DataFrame(data, dtype=float)

df.to_csv(numpy_file.replace('npy', 'tsv')), sep='\t', index=False, float_format='%.n', header=False)
