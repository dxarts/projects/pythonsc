import numpy as np
import os
import argparse
import pandas as pd

parser = argparse.ArgumentParser(description='Send SuperCollider Data to Numpy')

parser.add_argument('sc_file', type=str,
                help='the path to the sc data file')
parser.add_argument('numpy_file', type=str,
                help='the path to the numpy data file')

args = parser.parse_args()

sc_file = args.sc_file
numpy_file = args.numpy_file

# FOR TESTING
# numpy_file = '/Users/dan/Projects/SoundMaps/test2/Data/Data.npy'
data = []
file = open(sc_file)
for line in file:
    event = eval(line)
    data.append(event)

data = np.save(numpy_file, data)
